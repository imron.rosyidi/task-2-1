dependencies untuk menjalankan automate:
1. Java
2. Cucumber
2. Appium :
- Install node
- install Appium
- Instal UIautomator2 driver
2. Android Studio:
   -Download Android Studio
- Set up Emulator
3. Intelij
- Download intelij
4. Git bash
5. Gradle dan Maven:
- Create project and setup directory stockbit
- create CucumberRunner class stockbit
- Create AndroidDriverInstance class stockbit
- Create AndroidDriverHooks class stockbit
- Create feature file stockbit
- create step definition stockbit
- create page object stockbit
- combine page object with step definition stockbit
7. Selenium